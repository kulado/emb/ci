module gitlab.com/kulado/emb/ci

go 1.12

require (
	github.com/Masterminds/semver v1.4.2
	github.com/aws/aws-sdk-go v1.23.0
	github.com/bobesa/go-domain-util v0.0.0-20180815122459-1d708c097a6a
	github.com/kulado/sqlxmigrate v0.0.0-20190527223850-4a863a2d30db
	github.com/go-playground/locales v0.12.1 // indirect
	github.com/go-playground/universal-translator v0.16.0 // indirect
	github.com/go-redis/redis v6.15.2+incompatible
	github.com/gobuffalo/logger v1.0.1 // indirect
	github.com/gobuffalo/packr/v2 v2.5.2
	github.com/golang/protobuf v1.3.2 // indirect
	github.com/google/go-cmp v0.3.1
	github.com/iancoleman/strcase v0.0.0-20190422225806-e506e3ef7365
	github.com/jmoiron/sqlx v1.2.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/leodido/go-urn v1.1.0 // indirect
	github.com/lib/pq v1.2.0
	github.com/onsi/ginkgo v1.8.0 // indirect
	github.com/onsi/gomega v1.5.0 // indirect
	github.com/pborman/uuid v1.2.0
	github.com/pkg/errors v0.8.1
	github.com/stretchr/testify v1.4.0 // indirect
	github.com/urfave/cli v1.21.0
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4
	golang.org/x/net v0.0.0-20190813141303-74dc4d7220e7 // indirect
	golang.org/x/sys v0.0.0-20190813064441-fde4db37ae7a // indirect
	google.golang.org/appengine v1.6.1 // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/validator.v9 v9.29.1
)
